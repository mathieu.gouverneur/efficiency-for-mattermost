# Efficiency for Mattermost

 ![demo](demo.jpg) 

**Team :** Léonore BARZANI, David BENITAH, Timoté GARCIA, Robin GINER, Ivo LOPES, Mathieu GOUVERNEUR, Mathieu BOUVET


## About the plugin

Telework has become the new norm, where the digital transformation of companies has been accelerated by the pandemic, chat systems have remained the same since their creation. The proliferation of digital applications, software and other solutions has increased considerably. We are facing a massive increase in information received.

Based on this observation, we wanted above all to put at the heart of our problem the dimension of the effectiveness of a collaborative online tool. Our plug-in “ EFFICIENCY” responds perfectly.

We want to allow the user to use a free collaborative work tool, based on an adaptive scoring system so that he can increase his productivity as well as his efficiency in his daily exchanges.

To remedy this problem, we have implemented a system that calculates the time spent on each message, which takes into account the interactions of this message (answers given, emoticons, ...) and which produces a score out of 10. Above 5 the message is considered important and is automatically archived in an independent menu that allows the user to access it at any time.

Our plugin was created to allow users to gain productivity gain.

In development we made the decision to write an algorithm in French, for a person wishing to contribute to the Github project. It will easily find the path taken for the creation of this plugin. The diagrams created will be used by contributors.

In addition, we were able to start developing this plugin based on the Mattermost plugin template which can be found here: https://github.com/mattermost/mattermost-plugin-starter-template

Once the plugin is installed, you will be able to find a customization made by the designers of our team. The aim of our solution is to simplify as much as possible the reading of information by proposing in V1. It is a summary of a conversation with a method of “scoring” which, once developed, will allow the most important messages to appear.


## Hearing

This plugin is intended for Mattermost users (companies, schools, associations, students) wishing to have a collaborative work tool, score based on the importance of each message and the interaction with the chat participants.

For more information on how to contribute to this plugin, visit the Configuration section.


## License

This repository is subject to the MIT License.


## Before you start 

We assume that: 
- You have a GitHub account
- You are a developer and are familiar with Mattermost.


## Configuration

### Step 1 : Project Clone

First, you must retrieve our source code.

```bash
$ git clone https://gitlab.com/mathieu.gouverneur/efficiency-for-mattermost/
```

### Step 2 : Enter the project directory

Once the git clone downloads, you’ll need to be in the right folder:

```bash
$ cd efficiency-for-mattermost
```

### Step 3 : Build the plugin

Type this command in the terminal to generate a tar.gz archive:

```bash
$ make dist
```

### Step 4 : Mattermost plugin

Import the tar.gz archive that was generated in the application’s dist folder in the Plugin section of the Mattermost admin space.


## Developers

In order to propose a functional plugin, we would like to have the help of a GoLand developer to process the back-end part of the plugin.

In addition, this developer will also have to be familiar with the Mattermost environment and be willing to make progress.
